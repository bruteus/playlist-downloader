<pre>
       ___            __    __  ______   
      /\_ \          /\ \  /\ \/\__  _\  
 _____\//\ \      __ \ `\`\\/'/\/_/\ \/  
/\ '__`\\ \ \   /'__`\`\ `\ /'    \ \ \  
\ \ \L\ \\_\ \_/\ \L\.\_`\ \ \     \ \ \ 
 \ \ ,__//\____\ \__/.\_\ \ \_\     \ \_\
  \ \ \/ \/____/\/__/\/_/  \/_/      \/_/
   \ \_\                                 
    \/_/                                 
</pre>

plaYT - download music from YouTube a lot faster, w/out size/time restrictions!

Have you ever felt overwhelmed by amount of music you have to download to your
tiny device? Have you ever wanted to open your terminal, type "darude
sandstorm" and almost instantly download your beloved track? And what about
creating a file with song names written in each line and download them all at
once? I know, sounds crazy, but here it is: plaYT!

It's an interactive ncurses application written in python3.5 which offers:

 - nice user interface,
 - quick way to select and download music from YouTube,
 - opportunity to finally stop using "yttomp3.com" type sites and maybe even
   your entire web browser!

Sounds good, right? I wrote a simple list of steps that you need to complete
in order to get things done. Just in case your IQ is below 50 and you didn't
understand words typed above:

1. Specify query (or file with queries)
2. Interactively select one of the top results returned by YouTube
3. Wait until download process is completed

Now, let me specify what do you need in order to run plaYT on your computer:

 - Linux operating system,
 - ncurses library,
 - python3.5+,
 - youtube-dl.

Please submit bug reports in the issues section.
Contact: linuxxxmaster@protonmail.com