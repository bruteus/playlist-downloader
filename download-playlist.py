#!/usr/bin/python3
from urllib.request import urlopen
from html.parser import HTMLParser


class VideoMetadata:
    def __init__(self):
        self.title = None
        self.duration = None
        self.link = None

    def isComplete(self):
        return self.title is not None and self.duration is not None and self.link is not None

    def __str__(self):
        return '{0} --- [{1}]'.format(self.title, self.duration)


class YouTubeMetadataExtractor(HTMLParser):
    """ A few notes about the YouTube's method to present video metadata:
    -> Title and link: <a> tag with `yt-uix-tile-link` class (`title` and `href` attribute)
    -> Duration: <span> tag with `video-time` class

    This class provides functionality to extract this data from YouTube search results HTML code.
    """

    def __init__(self, top=5):
        super().__init__()
        self.top = top
        self.videos = []
        self.curr_video = VideoMetadata()
        self.read_duration = False


    def isDuration(self, tag, attrs):
        if tag == 'span':
            for attr in attrs:
                if attr[0] == 'class' and attr[1] == 'video-time':
                    return True
        return False


    def isTitleAndLink(self, tag, attrs):
        if tag == 'a':
            for attr in attrs:
                if attr[0] == 'class' and 'yt-uix-tile-link' in attr[1]:
                    return True
        return False


    def handle_starttag(self, tag, attrs):
        if self.curr_video.isComplete() and self.top:
            if '&list=' not in self.curr_video.link:  # if link is NOT a playlist, save it
                self.videos.append(self.curr_video)
                self.top -= 1
                self.curr_video = VideoMetadata()

        if self.isTitleAndLink(tag, attrs):
            for attr in attrs:
                if attr[0] == 'title':
                    self.curr_video.title = attr[1]
                elif attr[0] == 'href':
                    self.curr_video.link = attr[1]

        if self.isDuration(tag, attrs):
            self.read_duration = True


    def handle_data(self, data):
        if self.read_duration:
            self.curr_video.duration = data
            self.read_duration = False



queries = []
base_url = 'https://www.youtube.com/results?search_query='
with open('playlist.txt', 'r') as playlist:
    for line in playlist:
        queries.append(line[:-1].replace(' ', '+'))

for query in queries:
    top = 5
    yt_extractor = YouTubeMetadataExtractor()
    yt_extractor.feed(urlopen(base_url + query).read().decode())

    for lp, v in enumerate(yt_extractor.videos):
        print('{0}. {1}'.format(lp+1, v))

    video_id = int(input('Choose video: '))
    if video_id >= 1 and video_id <= top:
        print('You have chosen: {0}'.format(yt_extractor.videos[video_id-1]))
