import curses
from threading import Lock


print_lock = Lock()


# Some constants
MAX_LIST_ENTRIES = 1000
TEXT_FIELD_MINIMUM_LENGTH = 4
PROGRESS_BAR_MINIMUM_LENGTH = 8
PROGRESS_BAR_INFORMATION_LENGTH = 6
VIEW_COLUMN_MINIMUM_INTERSPACE_LENGTH = 1
VIEW_NUMBER_DELIMITER = '. '
VIEW_MINIMUM_HEIGHT = 1
VIEW_MINIMUM_WIDTH = len(str(MAX_LIST_ENTRIES) + VIEW_NUMBER_DELIMITER) + VIEW_COLUMN_MINIMUM_INTERSPACE_LENGTH + TEXT_FIELD_MINIMUM_LENGTH + PROGRESS_BAR_MINIMUM_LENGTH


class DownloadListView:
    def __init__(self, parent_window, model):
        #self.parent_window = parent_window
        parent_y, parent_x = parent_window.getyx()
        parent_height, parent_width = parent_window.getmaxyx()

        self.INITIAL_HEIGHT = self.height = parent_height - 2
        self.INITIAL_WIDTH = self.width = parent_width - 2
        self.starty = parent_y + 2
        self.startx = parent_x + 2

        self.calculate_lengths()

        self.win = curses.newwin(self.height, self.width, self.starty, self.startx)
        self.win.clear()
        self.win.refresh()

        model.view = self

        self.model = model
        self.active_item = 1
        self.top_item = 1

        self.error_state = False

        self.update()


    def calculate_lengths(self):
        progress_bar_percent = 0.35
        text_field_percent = 0.65

        remaining_space = self.width - len(str(MAX_LIST_ENTRIES)) - len(VIEW_NUMBER_DELIMITER) - VIEW_COLUMN_MINIMUM_INTERSPACE_LENGTH
        
        p_width = int(remaining_space * progress_bar_percent)
        if p_width % 2 != 0:
            p_width -= 1
        t_width = remaining_space - p_width
        
        self.text_field_width = t_width
        self.progress_bar_width = p_width


    @property
    def bottom_item(self):
        if self.top_item + self.items_fit_on_page >= self.total_items:
            return self.total_items
        elif self.items_fit_on_page >= self.total_items:
            return total_items
        else:
            return self.top_item + self.items_fit_on_page


    @property
    def total_items(self):
        return len(self.model.items)


    @property
    def items_fit_on_page(self):
        return self.height


    def _show(self):
        if self.total_items == 0 or self.error_state is True:
            self.win.clear()
            self.win.refresh()
            return

        for item_id in range(self.top_item, self.bottom_item + 1):
            if item_id == self.active_item:
                self.draw_row(item_id, active = True)
            else:
                self.draw_row(item_id)

        self.win.refresh()


    def update(self):
        if self.active_item > self.total_items:
            self.active_item = self.total_items
        self.win.clear()
        self._show()


    def normalize_text(self, text):
        if len(text) > self.text_field_width:
            return text[:self.text_field_width - 3] + '...'
        else:
            return text


    def progress_bar_text(self, value):
        wing_len = int((self.progress_bar_width - PROGRESS_BAR_INFORMATION_LENGTH - 2) / 2)
        if isinstance(value, int):
            if value < 10:
                return '{0}- {1}% -{0}'.format(' ' * wing_len, value)
            else:
                return '{0}-{1}%-{0}'.format(' ' * wing_len, str(value).rjust(3))
        elif isinstance(value, bool):
            return '{0}CONVRT{0}'.format(' ' * wing_len)                


    def draw_row(self, item_id, active = False):
        text = '{} {}'.format((str(item_id) + VIEW_NUMBER_DELIMITER).ljust(len(str(MAX_LIST_ENTRIES)) + len(VIEW_NUMBER_DELIMITER)), self.normalize_text(self.model.items[item_id - 1].text))
        percent = self.model.items[item_id - 1].percent
        converting = self.model.items[item_id - 1].converting
        
        if converting is False:
            progress_bar_text = self.progress_bar_text(percent)
        else:
            progress_bar_text = self.progress_bar_text(converting)
            
        filled_fields = int(self.progress_bar_width * (percent / 100))

        curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLACK)
        curses.init_pair(2, curses.COLOR_BLACK, curses.COLOR_WHITE)
        curses.init_pair(3, curses.COLOR_BLUE, curses.COLOR_BLACK)
        curses.init_pair(4, curses.COLOR_YELLOW, curses.COLOR_BLUE)

        if active is False:
            normal_color = 1
            filled_color = 2
        else:
            normal_color = 3
            filled_color = 4

        vertical_position = item_id - self.top_item

        with print_lock:
            # draw text
            self.win.addstr(vertical_position, 0, text, curses.color_pair(normal_color))

            # draw progress bar
            self.win.addch(vertical_position, self.width - self.progress_bar_width, '[', curses.color_pair(normal_color))
            self.win.addstr(progress_bar_text[:filled_fields], curses.color_pair(filled_color))
            self.win.addstr(progress_bar_text[filled_fields:] + ']', curses.color_pair(normal_color))

        self.win.refresh()


    def _scroll_up(self):
        if self.top_item != 1:
            self.top_item -= 1


    def _scroll_down(self):
        if self.bottom_item != self.total_items:
            self.top_item += 1


    def active_item_up(self):
        if self.active_item == 1:
            return

        if self.active_item == self.top_item:
            self._scroll_up()

        self.active_item -= 1
        self.update()


    def active_item_down(self):
        if self.active_item == self.total_items:
            return

        if self.active_item == self.bottom_item:
            self._scroll_down()

        self.active_item += 1
        self.update()


    def resize_event(self):
        new_height, new_width = self.win.getmaxyx()
        try:
            self.check_size(new_height, new_width)
        except ListViewInvalidSize:
            self.error_state = True

        if self.error_state == True:
            self.error_state = False

        self.height = new_height
        self.width = new_width
        self.calculate_lengths()
        self.update()
        

    def check_size(self, height, width):
        if height < VIEW_MINIMUM_HEIGHT or width < VIEW_MINIMUM_WIDTH:
            raise ListViewInvalidSize



class DownloadListModel:
    def __init__(self):
        self.items = []
        self.view = None


    def add(self, text):
        new_item = DownloadListItem(text)
        self.items.append(new_item)
        self.view.update()
        return new_item


    def delete(self, item):
        del self.items[self.items.index(item)]
        self.view.update()


    def update(self, item, value):
        if isinstance(value, int):
            assert value >= 0 and value <= 100, 'Percent value must fit in <0, 100>'
            self.items[self.items.index(item)].percent = value
        elif isinstance(value, bool):
            self.items[self.items.index(item)].converting = value

        self.view.update()


    def get_item_id(self, item):
        return self.items.index(item) + 1


class DownloadListItem:
    def __init__(self, text):
        self.text = text
        self.percent = 0
        self.converting = False
