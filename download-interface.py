''#!/usr/bin/python3
import subprocess
import threading
import re
import curses
from DownloadList import DownloadListModel as ListModel, DownloadListView as ListView, print_lock


class DownloadItem:
    def __init__(self, link, model_hook):
        self.link = link
        self.model_hook = model_hook
        self.list_item = None
        self.buff = ''


    def start_download(self):
        self.result = subprocess.Popen(['youtube-dl', '--newline', '-x', '--audio-format', 'mp3', self.link], stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
        self.capture()


    def capture(self):
        while True:
            next_char = self.result.stdout.read(1).decode()
            if next_char == '\n':
                self.analyze()
                self.buff = ''
            else:
                self.buff += next_char

            if next_char == '':  # EOF
                self.model_hook.delete(self.list_item)
                del self.list_item
                break


    def analyze(self):
        if self.buff == '':
            return False

        line_type = self.guess_line_type(self.buff)
        if line_type == 'start':
            text = re.search('(?<=Destination: ).+(?=\..+)', self.buff).group(0)
            self.list_item = self.model_hook.add(text)
        elif line_type == 'progress':
            percent = int(float(re.search('\d+\.\d+(?=% of)', self.buff).group(0)))
            self.model_hook.update(self.list_item, percent)


    def guess_line_type(self, line):
        line_types = {
            '\[download\] Destination: .+\.webm': 'start',
            '\[download\]\s+\d+\.\d+% of \d+\.\d+\S+ at .+ ETA \d+:\d+': 'progress',
            '\[download\] 100% of .+ in \d+:\d+': 'end'
        }

        for expr, t in line_types.items():
            if re.fullmatch(expr, line):
                return t
        return None



def keyboard_event_loop(dlv, debugwin):
    while True:
        dlv.win.keypad(True)
        key = dlv.win.getkey()
        debugwin.clear()
        if key in ('KEY_UP', '\x10', 'k'):
            dlv.active_item_up()
        elif key in ('KEY_DOWN', '\x0e', 'j'):
            dlv.active_item_down()
        with print_lock:
            debugwin.addstr(1, 1, str(key))
            debugwin.refresh()
    

def main(stdscr):
    stdscr.clear()
    curses.curs_set(0)

    height, width = stdscr.getmaxyx()

    win1 = curses.newwin(20, width // 2 - 4, 1, 1)
    win2 = curses.newwin(20, width // 2 - 4, 1, width // 2 - 2)
    win1.box()
    win2.box()
    win1.scrollok(True)
    win2.scrollok(True)

    stdscr.refresh()
    win1.refresh()
    win2.refresh()

    model = ListModel()
    view = ListView(win1, model)

    link1 = 'https://www.youtube.com/watch?v=pvyLH9rIbJU'
    link2 = 'https://www.youtube.com/watch?v=JUFzqvlplgg'
    dp1 = DownloadItem(link1, model)
    dp2 = DownloadItem(link2, model)
    t1 = threading.Thread(target=dp1.start_download)
    t2 = threading.Thread(target=dp2.start_download)
    t3 = threading.Thread(target = keyboard_event_loop, args = (view, win2), daemon = True)
    t1.start()
    t2.start()
    t3.start()

    
    t1.join()
    t2.join()

    win1.clear()
    win1.addstr(1, 1, 'Done!')
    win1.refresh()

    stdscr.getch()
    stdscr.clear()


curses.wrapper(main)
